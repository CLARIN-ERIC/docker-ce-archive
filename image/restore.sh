#!/bin/sh

RESTORE_FILE_NAME=$1
_BACKUP_DIR=${BACKUP_DIR:-/backups}
WORK_DIR="/data"

echo "Extracting backup archive ("${_BACKUP_DIR}/${RESTORE_FILE_NAME}")"
tar -xf "${_BACKUP_DIR}/${RESTORE_FILE_NAME}" -C "${WORK_DIR}" && chown -R apache:www-data /data