#!/bin/bash

if [ ! -f /accounts/.htpasswd ]; then
    echo "Generating .htpasswd file"
    if [ -z "${OFFICE_PASSWORD}" ]; then
        /accounts.sh --username "office"
    else
        /accounts.sh --username "office" --password "${OFFICE_PASSWORD}"
    fi
fi

echo "PID file cleanup" && rm -rf /var/run/apache2/httpd.pid
