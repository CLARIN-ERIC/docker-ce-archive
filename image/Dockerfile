#FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-base:2.0.1
FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-base:3.5.20

ENV BACKUP_DIR="/backups"
ARG VERSION_APACHE2="2.4.58-r0"
#"openssl==1.0.2t-r0" \
#RUN apk update \
RUN apk add --no-cache \
    "mksh==59c-r4" \
    "libssl3==3.1.4-r5" \
    "libcrypto3==3.1.4-r5" \
    "apache2==${VERSION_APACHE2}" \
    "apache2-utils==${VERSION_APACHE2}" \
    "apache2-ssl==${VERSION_APACHE2}" \
    "ca-certificates==20230506-r0" \
    "rsync==3.2.7-r4" \
    "util-linux==2.38.1-r8" \
    "shadow==4.13-r4" \
 && usermod -u 500 apache

COPY httpd.conf /etc/apache2/httpd.conf
COPY supervisor/apache2.conf /etc/supervisor/conf.d/apache2.conf
COPY fluentd/apache2.conf /etc/fluentd/conf.d/apache2.conf
COPY fluentd/dropbox_copy.conf /etc/fluentd/conf.d/dropbox_copy.conf
COPY init.sh /init/init.sh

#Apach2 vhost
COPY 000-default.conf /etc/apache2/conf.d/000-default.conf

#CE Archive cgi scripts
COPY web/clarin.css /var/www/html/css/clarin.css
COPY web/clarin_header.jpg /var/www/html/img/clarin_header.jpg
COPY web/cgi-bin/template.ksh /var/www/html/cgi-bin/template.ksh
COPY web/cgi-bin/get_ce_docs.ksh /var/www/html/cgi-bin/get_ce_docs.ksh
COPY web/cgi-bin/get_ce_documents.ksh /var/www/html/cgi-bin/get_ce_documents.ksh
COPY web/cgi-bin/cgilib.ksh /var/www/html/cgi-bin/cgilib.ksh
COPY web/cgi-bin/environment.ksh /var/www/html/cgi-bin/environment.ksh
COPY web/cgi-bin/get_ce_number.ksh /var/www/html/cgi-bin/get_ce_number.ksh
COPY web/cgi-bin/logevent.ksh /var/www/html/cgi-bin/logevent.ksh
COPY web/cgi-bin/wrapclarintoday.ksh /var/www/html/cgi-bin/wrapclarintoday.ksh
COPY web/how-to-get-a-ce-number.txt /var/www/html/how-to-get-a-ce-number.txt
COPY accounts.sh /accounts.sh
#COPY dropbox_copy.sh /dropbox_copy.sh
#COPY dropbox_copy_wrapper.sh /dropbox_copy_wrapper.sh
COPY backup.sh /backup.sh
COPY restore.sh /restore.sh

RUN mkdir -p /var/lock/apache2 &&\
    mkdir -p /var/run/apache2 && \
    mkdir -p /accounts /backups && \
    mkdir -p /data/documents && \
    echo "1" >> /data/documents/.CE_Number.txt && \
    touch /data/documents/.CE_List.txt && \
    chown -R apache:www-data /var/www/html && \
    chown -R apache:www-data /data && \
    chown -R apache:www-data /accounts && \
    chmod 0700 -R /data/documents && \
    chmod 0600 /data/documents/.CE_Number.txt && \
    chmod 0600 /data/documents/.CE_List.txt && \
    chmod 0500 /var/www/html/cgi-bin/*.ksh && \
    chmod ugo+x /accounts.sh \
 && chmod ugo+x /backup.sh \
 && chmod ugo+x /restore.sh \
 && chown -R apache:www-data /var/log/apache2 \
 && mv /usr/bin/sort /usr/bin/sort.orig \
 && ln -s /bin/busybox /usr/bin/sort

#mkdir -p /var/log/dropbox_copy && \
# mkdir -p /dropbox && \
# && chown apache:www-data dropbox_copy.sh /dropbox_copy_wrapper.sh /var/log/dropbox_copy /dropbox \
# && chmod ugo+x /dropbox_copy.sh /dropbox_copy_wrapper.sh \

#RUN mkdir -p /etc/cron.d \
# && echo "*/5 * * * * sh /dropbox_copy_wrapper.sh" >> /etc/cron.d/apache \
# && sed -i "s/crond -f -L \/var\/log\/crond.log/crond -f -L \/var\/log\/crond.log -l 4 -c \/etc\/cron.d/g" /etc/supervisor/conf.d/crond.conf

EXPOSE 80
EXPOSE 443

VOLUME "/data"
VOLUME "/accounts"
VOLUME "/backups"
VOLUME "/dropbox"
