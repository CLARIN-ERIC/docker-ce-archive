#!/bin/bash

FILE="/accounts/.htpasswd"
USERNAME=""
PASSWORD=""


#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --username)
        USERNAME=$2
        shift
        ;;
    --password)
        PASSWORD=$2
        shift
        ;;
    -h|--help)
        HELP=1
        ;;
    *)
        echo "Unkown option: $key"
        HELP=1
        ;;
esac
shift # past argument or value
done

#
# Execute based on mode argument
#
if [ "${HELP}" == "1" ]; then
    echo ""
    echo "accounts.sh --username <value> (--password <value>)"
    echo ""
    echo "  --username    Supply the username"
    echo "  --password    (optional) Supply a password, otherwise a random password will be generated."
    echo ""
    echo "  -h, --help    Show help"
    echo ""
    exit 1
fi

#Generate random password if needed
if [ "${PASSWORD}" == "" ]; then
    PASSWORD=$(head /dev/urandom | env LC_CTYPE=C tr -dc A-Za-z0-9 | head -c 32 ; echo '')
    echo "Generated password: ${PASSWORD}"
fi

if [ ! -f "${FILE}" ]; then
    htpasswd -b -c /accounts/.htpasswd "${USERNAME}" "${PASSWORD}"
else
    htpasswd -b /accounts/.htpasswd "${USERNAME}" "${PASSWORD}"
fi