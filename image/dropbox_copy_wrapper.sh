#!/bin/bash
rm -f /var/log/dropbox_copy/*.log
TIMESTAMP=$(date +"%Y-%m-%d_%H-%M-%S")
LOG_FILE="/var/log/dropbox_copy/dropbox_copy.log"
/dropbox_copy.sh >> "${LOG_FILE}" 2>&1 || (echo "cron job (/dropbox_copy.sh) failed with exit status $?"; cat "${LOG_FILE}")