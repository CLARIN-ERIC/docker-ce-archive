#!/bin/mksh
# get_ce_docs -- 
# version 27 ls3 09-08-2012 20-09-2015 (version 26 19-09-2015)

. /var/www/html/cgi-bin/environment.ksh
. $CE_SCRIPTS/cgilib.ksh
. $CE_SCRIPTS/template.ksh

# this is where the docs are mirrored

# this is the alias for the directory in the URL
UR=/v

# this is the query string, should start with CE-
QS=${QUERY_STRING:-$1}

# here we delete the bit that should stop people from
# sharing the search string with others
QS=${QS%NEVER*}

# go to the docs
cd $CE_ARCH

# print HTTP header
header

{ # list docs with right CE-number and select relevant fields

openpage

case ${QS} in
CE*|ce*) ls -lt ${QS}* 2> /dev/null |
     /usr/bin/awk  '{ $6="#"$6 ; $9="#"$9 ; print }' |
     /usr/bin/awk -v QS=${QS} -v FS="#" -v UR=${UR} '
     BEGIN { print "<table>"
             print "<tr><th>document date</th><th>filename</th></tr>" 
	     UNAV="<tr><td>not (yet) available:</td><td>&nbsp;&nbsp;&nbsp;"QS"</td></tr>"
	   }
           { UNAV=""
	     # print filename with link to doc
	     print "<tr><td>"$2"</td><td><a href="UR"/"$3">"$3"</a></td></tr>" 
	   } 
       END { print UNAV
             print "</table>"
           } '
     ;;
  *) ;;
esac

closepage

} | $CE_SCRIPTS/wrapclarintoday.ksh -t "List versions of ${QS} - <font color=\"red\">please share only links to individual documents with non BoD members</font>"
