#!/bin/mksh
# get_ce_number --
# version 86 ls3 09-08-2012 20-09-2015 (version 85 20-09-2015)

# some basic variables and functions
. /var/www/html/cgi-bin/environment.ksh
. $CE_SCRIPTS/cgilib.ksh


getcgiargs

. $CE_SCRIPTS/logevent.ksh -a

# runs always on PCM1876:
#CEBASE=c:/users/steven/dropbox/clarin-eu/ce_documents

# files with doc titles and running number:
CELIST=${CE_BASE}/.CE_List.txt
CENUM=${CE_BASE}/.CE_Number.txt

# current year:
CEYEAR=$(date +%Y)

# pathological link to discourage people to share
# the search link with others, as this would give tham access
# to all drafts and versions of this doc number:
UR="/cgi-bin/get_ce_docs.ksh?"
POSTUR="NEVER_EVER_USE_THIS_SEARCH_LINK_TO_REFER_TO_INDIVIDUAL_VERSIONS_OF_A_DOCUMENT"

# form to enter author and title:
function printnumberform {
  echo "<br>
  [<a href=/cgi-bin/get_ce_number.ksh><b>Re-display list</b></a>]
  <form action=/cgi-bin/get_ce_number.ksh method=\"post\">
  <input type=\"hidden\" name=\"NEWDOC\" value=\"$CEPREF\">
  Add new document
  $CEPREF <select name=\"AUTHOR\">
  <option value=\"none\">select author</option>
  <option value=\"CD\">CD</option>
  <option value=\"DB\">DB</option>
  <option value=\"BM\">BM</option>
  <option value=\"DF\">DF</option>
  <option value=\"DVU\">DVU</option>
  <option value=\"FdJ\">FdJ</option>
  <option value=\"FF\">FF</option>
  <option value=\"IvdL\">IvdL</option>
  <option value=\"JM\">JM</option>
  <option value=\"KB\">KB</option>
  <option value=\"NP\">NP</option>
  <option value=\"QB\">QB</option>
  <option value=\"SK\">SK</option>
  <option value=\"TK\">TK</option>
  <option value=\"CAC\">CAC</option>
  <option value=\"SCCTC\">SCCTC</option>
  <option value=\"CLIC\">CLIC - legal</option>
  <option value=\"CSC\">CSC - standards</option>
  <option value=\"SAB\">SAB</option>
  <option value=\"UIC\">UIC</option>
  <option value=\"in\">incoming</option>
  <option value=\"hard\">incoming - hard copy</option>
  <option value=\"scan\">incoming - scanned</option>
  </select> <input name=\"TITLE\" size=\"60\"> <input type=\"submit\" value=\"Add\"> <input type=\"reset\" value=\"Reset\">
  </form>
"
                       }

function printtopline {
  echo "[<a href=/cgi-bin/get_ce_number.ksh><b>Re-display list</b></a>]"
  echo "[<a href=/cgi-bin/get_ce_number.ksh?new><b>Request new CE number</b></a>]"
  echo "[<a href=/pc/clarin_archive/how-to-get-a-ce-number.txt><b>How does this work?</b></a>]"
  echo "[<a href=/pc/clarin_archive/check_archive.html><b>Check differences between archives</b></a>]"

                       }


function printsearchline {
 echo "<br><form action=/cgi-bin/get_ce_number.ksh method=\"post\">
       <select name=\"YEAR\">
       <option value=\"\">all years</option>
       <option value=\"2015\">2015</option>
       <option value=\"2014\">2014</option>
       <option value=\"2013\">2013</option>
       <option value=\"2012\">2012</option>
       <option value=\"2011\">2011</option>
       </select>
       <input name=\"TOPIC\"><input value=\"search titles\" type=\"submit\">
       <input type=\"hidden\" name=\"SEARCH\" value=\"on\">
       (regexp, case insensitive)
       </form>"
                         }

DAT=$(date +"%b %d")

case $QUERY_STRING in
      new) ACTION=new ;;
 personal) ACTION=personal ;;
[a-zA-Z]*) ACTION=list; SHOWTOPIC="${QUERY_STRING}"  ;;
     20??) ACTION=list; SHOWYEAR=CE-${QUERY_STRING} ;;
        *) ACTION=list ;;
esac

case $F_SEARCH in
  on) SHOWYEAR=CE-${F_YEAR} ; SHOWTOPIC="${F_TOPIC}"
      . $CE_SCRIPTS/logevent.ksh -m "[${F_YEAR:-xxall}][$F_TOPIC]"
      ;;
   *) ;;
esac

# get new number
typeset -Z4 CENEXT=$(cat $CENUM)

header

{

case "$F_TITLE" in
?*) # non-empty title encountered
    . $CE_SCRIPTS/logevent.ksh -m "[${F_AUTHOR}][${F_TITLE}]"
    NEWITEM="$F_NEWDOC	$DAT	$F_AUTHOR	$F_TITLE"
    # check if already present, eg by resubmit form
    grep -q "$NEWITEM" $CELIST ||
    { # if really new add, and update number
      echo "$NEWITEM" >> $CELIST
      CENEXT=$(( $CENEXT + 1 ))
      echo $CENEXT > $CENUM
      cp $CELIST $CENUM $CE_ARCH
    }
    ;;
 *) ;;
esac

CEPREF=CE-${CEYEAR}-${CENEXT}

# repeat bottom lines here

[ $ACTION = new ] && printnumberform  ||
{ printtopline
  printsearchline
}

# checksolis just checks whether the machine is connected to Solis domain
# as it sometimes loses the connection and may not automatically reconnect

checksolis ||
echo "<p><font color=\"red\"><b>WARNING $(date): archive synchronisation may not work properly</b></font>"

echo "<p>Current list (in reverse order):<br>
<br>
<table>
<tr>
<th>document id&nbsp;&nbsp;</th>
<th>date&nbsp;&nbsp;&nbsp;</th>
<th>author</th>
<th>description</th>
<th>document id&nbsp;&nbsp;</th>
<th>date&nbsp;&nbsp;&nbsp;</th>
<th>author</th>
<th>description</th>
</tr>"

/usr/bin/sort -r $CELIST |
/usr/bin/awk -v SHOWTOPIC="${SHOWTOPIC}" -v LINEAR=$LINEAR -v SHOWYEAR=$SHOWYEAR -v UR="$UR" -v POSTUR="$POSTUR"  -v FS="	" -v DD="</td><td>" -v DDA="</td><td align=\"center\">" '
                  BEGIN  { EMPTYITEM="<td colspan=\"4\"></td>"
                           SHOWTOPIC=tolower(SHOWTOPIC)
                         }

                  /^#/   { next }

          $1 !~ SHOWYEAR { next }
tolower($4) !~ SHOWTOPIC { next }

                         { DOCNR=substr($1,9,4)
                           ODD=(DOCNR+0)%2
                           DOCYR=substr($1,4,4)
                           # if (LDOCYR=="") LDOCYR=DOCYR
			   # if new year print last line as is
			   # and print horizontal line
	                   if (LDOCYR!=DOCYR)
	                   { if (SIT==1) print "<tr>" ITEM1 ITEM2 "</tr>"
	                     LDOCYR=DOCYR
	                     print "<tr><td colspan=\"8\"><hr></td></tr>"
			     # SIT=2 if next item has to start left
			     # SIT=0 if next item has to start right
			     # SIT=1 if empty first slot can be filled
	                     if (ODD==1) SIT=2
	                     else SIT=0
	                   }
                         }

                  SIT==2 { # odd numbers in new year always start left
		           # and empty right, and can be printed
                           ITEM1="<td><a href=" UR $1 POSTUR ">" $1 "</a>" DD $2 DDA $3 DD $4"</td>"
                           ITEM2=EMPTYITEM
                           LINE="<tr>" ITEM1 ITEM2 "</tr>"
                           print LINE
                           SIT=0
                           next
                         }

                  SIT==0 { # even numbers always start right and empty left
		           # but no printing yet
		           ITEM2="<td><a href=" UR $1 POSTUR ">" $1 "</a>" DD $2 DDA $3 DD $4"</td>"
                           ITEM1=EMPTYITEM
                           SIT=1; next
                         }

                  SIT==1 { # left slot can be filled with next (odd) item
		           # then print
		           ITEM1="<td><a href=" UR $1 POSTUR ">" $1 "</a>" DD $2 DDA $3 DD $4"</td>"
                           LINE="<tr>" ITEM1 ITEM2 "</tr>"
	                   print LINE
	                   SIT=0
                         }

                     END { # unprinted last line waiting
		           if (SIT==1) print "<tr>" ITEM1 ITEM2 "</tr>"
                           print "</table><hr><p>"
			 } '

[ $ACTION = new ] && printnumberform  ||
{ printtopline
  printsearchline
}

# if ready it is sent through the wrapper to make it a clarin page old style
} | $CE_SCRIPTS/wrapclarintoday.ksh -t "CE Archive - <font color=\"red\">please do not send any of the links on this page to internal or external people as they also show all drafts </font>"

# logevent.ksh logs everything happening on the cgi server
. $CE_SCRIPTS/logevent.ksh -z

