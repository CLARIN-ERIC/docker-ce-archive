#!/bin/mksh

function openpage {
    echo "
	<html>
	    <head>
	        <title>CE Archive</title>
  	        <link rel="stylesheet" href="/css/clarin.css" type="text/css">
	    </head>
	    <body>
		<div class="header" id="header"></div>
		<div class="main" id="main">
    "
}

function closepage {
    echo "
		</div>
	        <div class="footer" id="footer"></div>
	    </body>
	</html>
    "
}
