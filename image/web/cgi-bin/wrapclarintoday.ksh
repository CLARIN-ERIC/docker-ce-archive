#!/bin/mksh
# wraptoday -- 
# version 77 pcm1876 02-04-2002 19-09-2015 (version 76 17-01-2014)

cat

# set -x

# emergency:

###. c:/usr/bat/environment
###
###SIGN=$(cat w:/incs/sign.inc.html)
###
###case $0 in
###  *wraptoday*) TEMPLATE="w:/incs/elsnet_wraptemplate.exp.html"
###	       ;;
###*wrapsktoday*) TEMPLATE="w:/incs/sk_wraptemplate.exp.html"
###               ;;
###*wrapsttoday*) TEMPLATE="w:/incs/todaysdsttemplate.html"
###               ;;
###*wrapettoday*) TEMPLATE="w:/incs/todaysdttemplate.html"
###               ;;
###*wrapfeltoday*) TEMPLATE="w:/incs/fel_wraptemplate.exp.html"
###               ;;
###*wrapclarintoday*) TEMPLATE="w:/incs/clarin_wraptemplate.exp.html"
###               ;;
###esac
###
###TITLE="(no title)"
###
###while [ $# -gt 0 ]
###do case $1 in
###   -t) shift
###       TITLE="$*"
###       break
###       ;;
###   -p) PRE=on
###       shift
###       ;;
###    *) break
###       ;;
###esac
###done
###
###set --
###
###awk -v SIGN="$SIGN" -v PRE=$PRE -v TITLE="${TITLE}" -v TEMPLATE="${TEMPLATE}" '
###     BEGIN  { while ( getline < TEMPLATE )
###              { gsub(/%%.*title.*%%/,TITLE,$0)
###	        if ($0~/%%.*%%/) 
###		{ STORE="yes" 
###		  gsub(/%%.*%%/,"",$0)
###                }
###		if (STORE=="yes")
###		{ K++ ; KEEP[K]=$0 }
###	        else print $0
###	      }
###	      if (PRE=="on") print "<pre>"
###	      # else print "<p>"
###	    }
###
###/sign.inc.html/ { # get rid of traces of SIGN in non-SSI
###                  sub(/#include.*sign\.inc\.html"/," end of form",$0) 
###		}
###
###/<\/[Ff][Oo][Rr][Mm]>/ { sub(/<\/[Ff][Oo][Rr][Mm]>/,SIGN"&",$0) }
###	    { print $0 }
###     END    { if (PRE=="on") print "</pre>"
###              # else print "<p>"
###              for (I=1;I<=K;I++)
###              print KEEP[I]
###	    } ' $*
###
