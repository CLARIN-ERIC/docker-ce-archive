#     
# environment -- specifies environment for sub-shells (cf .cshrc)

# general settings

set -a

# PATH variable
export CE_BASE="/data/documents"
export CE_SCRIPTS="/var/www/html/cgi-bin"
export CE_ARCH="/data/documents"
# ensures that variables set in left hand side of pipes
# are also available on the right hand side
export TK_SH_RHS_OF_PIPE_IN_CURRENT_SHELL=1
# case statement is case sensitive
DUALCASE=on
