#!/bin/mksh
# cgilib -- 
# version 165 pcm1876 10-05-2002 20-09-2015 (version 164 20-09-2015)

# contains
# - header
# - getcgiargs
# - checksolis
#
# variables generated here have prefix HT_
#

HT_CGIPREFIX=${CGIPREFIX:-F_}    # prefix for form variables
HT_QQ='&'                        # ampersand sign
HT_QS=$QUERY_STRING              # short name for quesry string

function getcgiargs {
# assigns values to cgi variables, all prefixed with F_
# better Perl scripts exist for this job
case $REQUEST_METHOD in
   post|POST) # cat to Q_S does not work because of 32K limit in windows
	      HT_ACT=post
	      ;;
   get|GET|*) HT_ACT=get
	      ;;
esac

eval $( { [ $HT_ACT = post ] && cat || print -n "${QUERY_STRING}"
          echo "${HT_QQ}"
        } | sed -e "s/\'/%27/g" | sed 's/\["'"';:\]//g; s/[+]/ /g" | 
        /usr/bin/awk -v CGIPREFIX=$HT_CGIPREFIX '
	    BEGIN { RS="&"; FS="=" ; ORS="" 
	          }
            $1~/^[a-zA-Z_-][a-zA-Z0-9_-]*$/ { 
                    printf "%s%s=%c%s%c\n", CGIPREFIX, $1, 39, $2, 39 
		  }
              END { print "" 
	          } ' 
      )

# end of getcgiargs
}

function checksolis { # check soliscom.ok, not relevant
            true
	  }

function header { # print standard http header
                  HT_PHEADER="Content-type:text/html\n\n$*" 
	          print "$HT_PHEADER"
                }

