#!/bin/bash

DROPBOX_DIR="/dropbox"
CE_DOC_UPLOADS_DIR="${DROPBOX_DIR}/CLARIN-EU/CE_Docs_Uploads"
CE_DOCUMENTS_DIR="${DROPBOX_DIR}/CLARIN-EU/CE_Documents"
WORK_DIR="/data/incoming"
OUTPUT_DIR="/data/documents"

TIMESTAMP=$(date +"%Y-%m-%d_%H-%M-%S")
echo ""
echo "======================================================================"
echo "= Running dropbox sync [${TIMESTAMP}]                          ="
echo "======================================================================"
#Move incoming data from dropbox to temporary folder
if [ ! -d "${WORK_DIR}" ]; then
	mkdir "${WORK_DIR}"
fi

#Process incoming data (replace spaces with underscores)
for file in ${CE_DOC_UPLOADS_DIR}/*; do
	if [ -f "$file" ]; then
	    echo "Pre processing: $file"
		mv "$file" `echo $file | tr ' ' '_'` ;
	fi
done

#Move files from uploads to incoming
echo "Moving files into the work area ${WORK_DIR}"

rsync -a --no-owner --no-group --remove-source-files --exclude="done" "${CE_DOC_UPLOADS_DIR}/" "${WORK_DIR}/"

#SED_SEARCH_PATTERN=$(echo ${CE_DOC_UPLOADS_DIR} | sed 's/\//\\\//g')
#SED_REPLACE_PATTERN=$(echo ${CE_DOC_UPLOADS_DIR}/ | sed 's/\//\\\//g')
#for file in ${CE_DOC_UPLOADS_DIR}/*; do
#	if [ -f "$file" ]; then
#	    echo "  Moving $file"
#		cp "$file" `echo $file | sed "s/${SED_SEARCH_PATTERN}/${SED_REPLACE_PATTERN}/g"` && rm $file
#	fi
#done

#Post processing
#Ensure CE numbers start with upper case CE-z
#echo "Post processing" && cd "${WORK_DIR}" && rename -v 's/^ce-(.+)/CE-$1/' *
(
    echo "Post processing"
    cd "${WORK_DIR}"
    for i in *; do
        if [ -f "${i}" ]; then
            mv "$i" "$(echo $i | sed 's/^ce-/CE-/')";
        fi
    done
)

echo "Rsyncing files into place"

echo "${WORK_DIR} --> ${OUTPUT_DIR}"
#Rsync new processed data from incoming to documents
rsync -av "${WORK_DIR}/" "${OUTPUT_DIR}/"

#And make sure symlinks to list and number files exist with proper caption
if [ ! -h "${OUTPUT_DIR}/.CE_List.txt" ]; then
	ln -s "${OUTPUT_DIR}/.ce_list.txt" "${OUTPUT_DIR}/.CE_List.txt"
fi
if [ ! -h "${OUTPUT_DIR}/.CE_Number.txt" ]; then
	ln -s "${OUTPUT_DIR}/.ce_number.txt" "${OUTPUT_DIR}/.CE_Number.txt"
fi

#Rsync webserver documents to dropbox
echo "${OUTPUT_DIR} --> ${CE_DOCUMENTS_DIR}"
rsync -av "${OUTPUT_DIR}/" "${CE_DOCUMENTS_DIR}/"

#Rsync upload documents to dropox done folder
echo "${WORK_DIR} --> ${CE_DOC_UPLOADS_DIR}/done/}"
rsync -av "${WORK_DIR}/" "${CE_DOC_UPLOADS_DIR}/done/"

#Cleanup incoming?
rm -r "${WORK_DIR}"