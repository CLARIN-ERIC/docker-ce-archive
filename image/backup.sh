#!/bin/bash

_BACKUP_DIR=${BACKUP_DIR:-/backups}
DATE=$(date +%Y%m%d_%H%M%S)

cd "${_BACKUP_DIR}" && tar -pczvf "ce_archive_${DATE}.tar.gz" -C /data .